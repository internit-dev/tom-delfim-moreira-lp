<section id="arquitetura" class="js-section-effect">
    <div class="container-fluid tom ">
        <div class="row tom_content">
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum">
                            <div class="tom_development-title">
                                <h1>No quadrilátero mais<br> cobiçado do Leblon.<br> </h1>
                            </div>
                            <div class="tom_development-text">
                                <p>In the most desirea area of leblon.<br></p>
                            </div>
                            <div class="tom_development-box">
                                <div class="tom_development-box-one">
                                    <div class="tom_development-box-one-title" data-toggle="modal" data-target="#modalMaps" >
                                        <h6>AV. DELFIM MOREIRA, 558</h6>
                                        <span>ENTRE JOÃO LIRA E JOSÉ LINHARES</span>
                                    </div>
                                    <a data-fslightbox="fachada" href="./assets/src/img/Fachada.jpg">
                                        <img src="./assets/src/img/fotografia-imovel.png" alt="Fotografia">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum js-box-scroll">
                            <div class="tom_development-box">
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/gesler-arquitetura.png" alt="Gesler Arquitetura">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-illustration-name">
                                        <p>Michel Rike</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>GENSLER</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Buscamos trazer a melhor expertise internacional em design com a Gensler, que assina projetos incríveis no mundo todo, e com o melhor da nossa cultura e realidade locais.</p>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box">
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/marcos-sa.png" alt="Marcos Sá">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-illustration-name">
                                        <p>Marcos Sá</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>SÁ E ALMEIDA PAISAGISMO</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Com uma paisagem tão marcante como a da praia do Leblon, só resta ao paisagismo reverenciá-la, fazendo o mínimo de intervenções, 
                                            de modo a respeitar a sua fruição e a continuidade do espaço.</p>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box reverse">
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>ERICK FIGUEIRA DE MELLO</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Usamos como premissa para a decoração o estilo sofisticado e despretensioso carioca.
                                            O desenho das áreas internas reproduz as curvas da fachada, inspiradas na orla da nossa cidade.</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/erick-figuereido.png" alt="ERICK FIGUEIRA DE MELLO">
                                        </a>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box mt-5">
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/viva-projects.png" alt="Cecília Tanure e Camilla Barella">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-illustration-name">
                                        <p>Cecília Tanure e Camilla Barella</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>VIVA PROJECTS</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>A VIVA idealizou para o empreendimento um projeto que transforma o residencial do Leblon, por meio da arte, em um marco urbano. 
                                            Os artistas escolhidos são os maiores expoentes do cenário artístico contemporâneo nacional e internacional.</p>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box">
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>SÉRGIO GATTÁS</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Trabalhar em parceria com a Gensler, um escritório internacional, trouxe a oportunidade de um projeto com um olhar novo e criativo. 
                                            Uma arquitetura de qualidade em um dos melhores endereços do Rio de Janeiro.
                                        </p>
                                    </div>
                                </div>
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/sergio-gattas.png" alt="SÉRGIO GATTÁS">
                                        </a>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box reverse">
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/ld_studios.png" alt="LD STUDIO">
                                        </a>
                                    </div>
                                </div>
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>LD STUDIO</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Poder conectar o movimento das ondas da fachada com toda a arte presente em cada ambiente coletivo é uma oportunidade única.</p>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>               
</section>