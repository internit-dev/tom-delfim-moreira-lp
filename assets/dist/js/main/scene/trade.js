const Trade = function(){
    this.sections = document.querySelectorAll('.js-section-effect')
    this.button = document.querySelector('.menu_content-contact-text span')
    this.menu = document.querySelectorAll('[data-section]')
    this.mark_water = document.querySelector('.water_mark img')
    this.status = 0;
    this.new;

    this.trade_color = ($target)=> {
        let tooltip;
        let menu = document.querySelectorAll("menu .menu_content-close-itens span");
        switch($target){
            case 0 :
                for(let item of this.menu){
                    item.style = "background: #1d1d1f;color: #fff;border: 1px solid #fff";
                    tooltip = item.closest("li")
                    tooltip.querySelector("span").style = "opacity: 0;transform: translateY(-1000px);"
                    tooltip.classList.remove("active")
                }
                this.menu[$target].style = " background: #fff;color: #393939 ;border: 1px solid  #393939";
                tooltip = this.menu[$target].closest("li")
                for(let span of menu){
                    span.style = "background: #fff";
                }
                tooltip.querySelector("span").style = "opacity: 1;transform: translateY(-16px);"
                tooltip.classList.add("active")
                this.mark_water.src = "./assets/src/img/logo/Logotipo_branco.svg";
                break;
            case 1 :
                for(let item of this.menu){
                    item.style = "background: #fff;color: #1d1d1f ;border: 1px solid #1d1d1f";
                    tooltip = item.closest("li")
                    tooltip.querySelector("span").style = "opacity: 0;transform: translateY(-1000px);"
                    tooltip.classList.remove("active")
                }
                for(let span of menu){
                    span.style = "background:  #1d1d1f";
                }
                this.menu[$target].style = "background: #1d1d1f;color: #fff ;border: 1px solid #fff";
                tooltip = this.menu[$target].closest("li")
                tooltip.classList.add("active")
                tooltip.querySelector("span").style = "opacity: 1;transform: translateY(-16px);background: #1d1d1f;color: #fff;"
                this.mark_water.src = "./assets/src/img/logo/Logotipo_branco.svg";
                break;
            case 2 :
                for(let item of this.menu){
                    item.style = "background: #1d1d1f;color: #fff;border: 1px solid #fff";
                    tooltip = item.closest("li")
                    tooltip.querySelector("span").style = "opacity: 0;transform: translateY(-1000px);"
                    tooltip.classList.remove("active")
                }
                for(let span of menu){
                    span.style = "background:  #fff";
                }
                this.menu[$target].style = " background: #fff;color: #393939 ;border: 1px solid  #393939";
                tooltip = this.menu[$target].closest("li")
                tooltip.classList.add("active")
                tooltip.querySelector("span").style = "opacity: 1;transform: translateY(-16px);background: #fff;color: #393939 ;"
                this.mark_water.src = "./assets/src/img/logo/Logotipo_preto.svg";
                break;
            case 3:
                for(let item of this.menu){
                    item.style = "background: #fff;color: #1d1d1f ;border: 1px solid #1d1d1f";
                    tooltip = item.closest("li")
                    tooltip.querySelector("span").style = "opacity: 0;transform: translateY(-1000px);"
                    tooltip.classList.remove("active")
                }
                for(let span of menu){
                    span.style = "background:  #1d1d1f";
                }
                this.menu[$target].style = "background: #1d1d1f;color: #fff ;border: 1px solid #fff";
                tooltip = this.menu[$target].closest("li")
                tooltip.classList.add("active")
                tooltip.querySelector("span").style = "opacity: 1;transform: translateY(-16px);background: #1d1d1f;color: #fff;"
                this.mark_water.src = "./assets/src/img/logo/Logotipo_branco.svg";
                break;
            case 4:
                for(let item of this.menu){
                    item.style = "background: #1d1d1f;color: #fff;border: 1px solid #fff";
                    tooltip = item.closest("li")
                    tooltip.querySelector("span").style = "opacity: 0;transform: translateY(-1000px);"
                    tooltip.classList.remove("active")
                }
                for(let span of menu){
                    span.style = "background:  #fff";
                }
                this.menu[$target].style = " background: #fff;color: #393939 ;border: 1px solid  #393939";
                tooltip = this.menu[$target].closest("li")
                tooltip.classList.add("active")
                tooltip.querySelector("span").style = "opacity: 1;transform: translateY(-16px);background: #fff;color: #393939 !;"
                this.mark_water.src = "./assets/src/img/logo/Logotipo_branco.svg";
                break;
            case 5:
                for(let item of this.menu){
                    item.style = "background: #fff;color: #1d1d1f ;border: 1px solid #1d1d1f";
                    tooltip = item.closest("li")
                    tooltip.querySelector("span").style = "opacity: 0;transform: translateY(-1000px);"
                    tooltip.classList.remove("active")
                }
                for(let span of menu){
                    span.style = "background:  #1d1d1f";
                }
                this.menu[$target].style = "background: #1d1d1f;color: #fff;border: 1px solid #fff";
                tooltip = this.menu[$target].closest("li")
                tooltip.classList.add("active")
                tooltip.querySelector("span").style = "opacity: 1;transform: translateY(-16px);background: #1d1d1f;color: #fff;"
                this.mark_water.src = "./assets/src/img/logo/Logotipo_branco.svg";
                break;
            case 6:
                for(let span of menu){
                    span.style = "background:  #fff";
                }
                for(let item of this.menu){
                    item.style = "background: #1d1d1f;color: #fff;border: 1px solid #fff";
                    tooltip = item.closest("li")
                    tooltip.querySelector("span").style = "opacity: 0;transform: translateY(-1000px);"
                    tooltip.classList.remove("active")
                }
                this.menu[$target].style = " background: #fff;color: #393939;border: 1px solid  #393939";
                tooltip = this.menu[$target].closest("li")
                tooltip.classList.add("active")
                tooltip.querySelector("span").style = "opacity: 1;transform: translateY(-16px);background: #fff;color: #393939 !;"
                this.mark_water.src = "./assets/src/img/logo/Logotipo_branco.svg";
                break;
        }
    }

    this.next = ()=> {
        this.trade_color(this.new);

        /**
         * Elemento Atual - left
         */
       this.sections[this.status].style = 'z-index: 0';
        if(this.status == 0){
            setTimeout(()=>{
                this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY("+(this.status + 1)+"00%)";
                this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
        }, 1)
    
        }else if(this.new == 4){
            if(this.status == 2){
                setTimeout(()=>{
                    this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(-100%)";
                    this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
                }, 1)
            }else if(this.status == 3){
                this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(-200%)";
                this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
            }else{
                setTimeout(()=>{
                    this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(00%)";
                    this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
                }, 1)
            }
            
        }else{
            setTimeout(()=>{
                this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(00%)";
                this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
            }, 1)
            
        }
      
      
      
      /**
       * Próximo Elemento - left
       */
       this.sections[this.new].style = 'z-index: 1';
       this.sections[this.new].querySelectorAll(".tom_content-collum")[0].style = "transform: translateY(-"+(this.new + 1)+"00%);";

        setTimeout(()=>{
            this.sections[this.new].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
            this.sections[this.new].querySelectorAll(".tom_content-collum")[0].style = "transform: translateY(-"+(this.new)+"00%)";
        }, 1)


        /**
         * Atual Elemento - Right
         */
            if(this.status == 0){
                setTimeout(()=>{
                    this.sections[this.status].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.status + 1)+"00%)";
                    this.sections[this.status].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
                }, 1)
            }else{
                setTimeout(()=>{
                this.sections[this.status].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new)+"00%)";
                this.sections[this.status].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
                }, 1)
            }

        /**
         * Próximo Elemento - Right
         */

        if(this.new >= 2 && this.status == 0 || this.new >= 2 && this.status == 1 || this.new >= 4 && this.status == 2 ){
            this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new - 1)+"00%);";
            setTimeout(()=>{
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new)+"00%)";
            }, 1)
       }else{
        this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.status)+"00%);";
        setTimeout(()=>{
            this.sections[this.new].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
            this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new)+"00%)";
        }, 1)
       }
            
        
    }
    this.prev = ()=>{
        this.trade_color(this.new);

        this.sections[this.status].style = 'z-index: 0';
        setTimeout(()=>{
            this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = "transform: translateY(-"+(this.status + 1)+"00%)";
            this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
        }, 1)
     
      
      
      /**
       * Próximo Elemento - left
       */
     
       setTimeout(()=>{
            this.sections[this.new].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
            this.sections[this.new].querySelectorAll(".tom_content-collum")[0].style = "transform: translateY(-"+(this.new)+"00%)";
        }, 1)


        
        /**
         * Atual Elemento - Right
         */
         
        if(this.new == 0){
            setTimeout(()=>{
                this.sections[this.status].querySelectorAll(".tom_content-collum")[1].style = " transform: translateY(00%)";
                this.sections[this.status].querySelectorAll(".tom_content-collum")[1].classList.add("transition");
            }, 1)
          
        }else{
            setTimeout(()=>{
                this.sections[this.status].querySelectorAll(".tom_content-collum")[1].style = " transform: translateY(-"+(this.status - 1)+"00%)";
                this.sections[this.status].querySelectorAll(".tom_content-collum")[1].classList.add("transition");
            }, 1)
          
        }
            

        /**
         * Próximo Elemento - Right
         */
        if(this.new == 0 && this.status == 1){
            this.sections[this.new].style = 'z-index: 1';
            this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new - 2)+"00%);";
    
            setTimeout(()=>{
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(00%)";
            }, 1)
        }else if(this.status == 2 && this.new == 0){
            console.log("oi")
            this.sections[this.new].style = 'z-index: 1';
            this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new - 1)+"00%);";
    
            setTimeout(()=>{
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new)+"00%)";
            }, 1)
        }else{
            this.sections[this.new].style = 'z-index: 1';
            this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new - 2)+"00%);";
    
            setTimeout(()=>{
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
                this.sections[this.new].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.new)+"00%)";
            }, 1)
        }
 
 

       
    }

    this.select_next = ($info)=>{

        this.trade_color($info);

        /**
         * Elemento Atual - left
         */
       this.sections[this.status].style = 'z-index: 0';
       if(this.status == 0){
           setTimeout(()=>{
               this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY("+(this.status + 1)+"00%)";
               this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
       }, 1)
   
       }else if(this.new == 4){
           if(this.status == 2){
               setTimeout(()=>{
                   this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(-100%)";
                   this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
               }, 1)
           }else if(this.status == 3){
               this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(-200%)";
               this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
           }else{
               setTimeout(()=>{
                   this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(00%)";
                   this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
               }, 1)
           }
           
       }else{
           setTimeout(()=>{
               this.sections[this.status].querySelectorAll(".tom_content-collum")[0].style = " transform: translateY(00%)";
               this.sections[this.status].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
           }, 1)
           
       }
     
     
     
     /**
      * Próximo Elemento - left
      */
      this.sections[$info].style = 'z-index: 1';
      this.sections[$info].querySelectorAll(".tom_content-collum")[0].style = "transform: translateY(-"+($info + 1)+"00%);";

       setTimeout(()=>{
           this.sections[$info].querySelectorAll(".tom_content-collum")[0].classList.add("transition")
           this.sections[$info].querySelectorAll(".tom_content-collum")[0].style = "transform: translateY(-"+($info)+"00%)";
       }, 1)


       /**
        * Atual Elemento - Right
        */
           if(this.status == 0){
               setTimeout(()=>{
                   this.sections[this.status].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+(this.status + 1)+"00%)";
                   this.sections[this.status].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
               }, 1)
           }else{
               setTimeout(()=>{
               this.sections[this.status].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+($info)+"00%)";
               this.sections[this.status].querySelectorAll(".tom_content-collum")[1].classList.add("transition")
               }, 1)
           }

       /**
        * Próximo Elemento - Right
        */

        this.sections[$info].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+($info - 1)+"00%);";
        setTimeout(()=>{
            this.sections[$info].querySelectorAll(".tom_content-collum")[1].classList.add("transition");
            this.sections[$info].querySelectorAll(".tom_content-collum")[1].style = "transform: translateY(-"+($info)+"00%)";
        }, 1)    
    }
}

export{
    Trade
}