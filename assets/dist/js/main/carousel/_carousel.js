let Carousel = function(){
    let $loc = new Array();

    this.swiper = function($coord, $obj){
        if(document.querySelector($coord)){
            document.addEventListener("DOMContentLoaded", function(){
                var swiper = new Swiper($coord, $obj);
                return swiper;
            });
        }
    }

    this.bootstrap = function( $obj, $coord = ".boot-carousel"){
        if(document.querySelector($coord)){
           document.addEventListener('DOMContentLoaded', ()=> {
                let carousel = document.querySelector($coord)
                let bootCarousel = new bootstrap.Carousel(carousel, $obj)
           })
        }
    }
}

let cmd = new Carousel();


// cmd.swiper({
//     // Optional parameters
//     direction: 'horizontal',
//     loop: true,

//     // If we need pagination
//     pagination: {
//         el: '.swiper-pagination',
//     },

//     // Navigation arrows
//     navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev',
//     },

//     // And if we need scrollbar
//     scrollbar: {
//         el: '.swiper-scrollbar',
//     },
// })




const thumb = cmd.swiper('.swiper-thumbnail-carousel',{
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: ".swiper-thumbnail-button-next",
        prevEl: ".swiper-thumbnail-button-prev",
    },
    
})
cmd.swiper('.swiper-galery-carousel',{
    spaceBetween: 10,
    thumbs: {
      swiper:  thumb,
    },
})

var thumb_2 = new Swiper(".swiper-thumbnail-galeria-imovel", {
    spaceBetween: 10,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesProgress: true,
    breakpoints: {
        0: {
          slidesPerView: 1,
        },
        550: {
          slidesPerView: 3,
        }
    },
    navigation: {
        nextEl: ".swiper-thumbnail-imovel-button-next",
        prevEl: ".swiper-thumbnail-imovel-button-prev",
    },
});

var galeria_imovel = new Swiper(".swiper-galeria-imovel", {
    spaceBetween: 10,
    thumbs: {
      swiper: thumb_2,
    },
    navigation: {
        nextEl: ".swiper-imovel-button-next",
        prevEl: ".swiper-imovel-button-prev",
    },
    breakpoints: {
        0: {
            spaceBetween: 0,
        },
        550: {
            spaceBetween: 10,
        }
    },
});


var thumb_3 = new Swiper(".swiper-thumbnail-planta-carousel", {
    spaceBetween: 10,
    slidesPerView: 3,
    freeMode: true,
    watchSlidesProgress: true,
    navigation: {
        nextEl: ".swiper-thumbnail-planta-button-next",
        prevEl: ".swiper-thumbnail-planta-button-prev",
    },
});

var galeria_planta = new Swiper(".swiper-planta-carousel", {
    spaceBetween: 10,
    thumbs: {
      swiper: thumb_3,
    },
    breakpoints: {
        0: {
            direction: "vertical",
            slidesPerView: 4,
            spaceBetween: 40,
            cssMode: true,
        },
        550: {
            direction: "horizontal",
        }
    },
});


export{
    Carousel
}