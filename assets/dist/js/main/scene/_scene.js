import { Trade } from "./trade.js";


let cmd = new Trade();
let $status = document.querySelector("[data-status]")
let order = false

let all = document.querySelectorAll("[data-section]")
if(window.innerWidth > 550){
    for(let itens of all){
        itens.removeAttribute("href")
    }
}

document.addEventListener("DOMContentLoaded", ()=>{
 

    if(window.innerWidth > 550){
        for(let item of cmd.menu){
            item.onclick = function(){
                let $status = document.querySelector("[data-status]")
                if( document.querySelector("menu").classList.contains("is--active")){
                    document.querySelector("menu").classList.remove("is--active")
                }
               if(cmd.status != this.dataset.section){
                    if(this.dataset.section > cmd.status){
                        cmd.new = parseInt(this.dataset.section)
                        cmd.next();
    
                        if((parseInt($status.dataset.status) + 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
    
                        setTimeout(()=>{
                            cmd.status = parseInt(this.dataset.section)
                            $status.dataset.status = parseInt(this.dataset.section)
                        },100)
                    }else{
                        cmd.new = parseInt(this.dataset.section);
                        cmd.prev();
    
                        if((parseInt($status.dataset.status) - 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
                        setTimeout(()=>{
                            cmd.status = parseInt(this.dataset.section)
                            $status.dataset.status = parseInt(this.dataset.section)
                        },100)
                    }
               }
            }
        }
    }else{
        for(let itens of all){
            itens.onclick = function (){
                document.querySelector("menu").classList.remove("is--active")
            }
        }
    }
   
    // cmd.button.onclick = function(){
    //     if(window.innerWidth > 550){
    //         cmd.select_next(6)
            
    //         document.querySelector(".mouse_scroll").style = "opacity: 0;"

    //         setTimeout(()=>{
    //             cmd.status = 6
    //         },100)
    //     }else{
    //         window.location.href = "#contact"
    //     }

    // }
    window.onwheel = function(ev){
      
        /**
         * Trade Slide 
         */
        if(order == false && window.innerWidth > 550){
            order = true
            setTimeout(()=>{
                let second = document.querySelector(".js-box-scroll")
                let terceiro =  document.querySelector(".js-box-scroll-2")

                if(ev.deltaY > 0 && cmd.status < 6){

                    if($status.dataset.status == 1 && second.scrollTop >= 1225.5999755859375){
                        cmd.new = parseInt($status.dataset.status) + 1
                        cmd.next();
                        if((parseInt($status.dataset.status) + 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
                        
                        setTimeout(()=>{
                            cmd.status =  parseInt($status.dataset.status) + 1 >= 7 ? 6 :  parseInt($status.dataset.status) + 1
                            $status.dataset.status = parseInt($status.dataset.status) + 1 >= 7 ? 6 :  parseInt($status.dataset.status) + 1
                        },100)
                    }else if($status.dataset.status == 2 && terceiro.scrollTop >= 825.5999755859375){
                        cmd.new = parseInt($status.dataset.status) + 1
                        cmd.next();
                        if((parseInt($status.dataset.status) + 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
                        
                        setTimeout(()=>{
                            cmd.status =  parseInt($status.dataset.status) + 1 >= 7 ? 6 :  parseInt($status.dataset.status) + 1
                            $status.dataset.status = parseInt($status.dataset.status) + 1 >= 7 ? 6 :  parseInt($status.dataset.status) + 1
                        },100)
                    }else if($status.dataset.status != 1 && $status.dataset.status != 2){
        
                        cmd.new = parseInt($status.dataset.status) + 1
                        cmd.next();
                        if((parseInt($status.dataset.status) + 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
                        
                        setTimeout(()=>{
                            cmd.status =  parseInt($status.dataset.status) + 1 >= 7 ? 6 :  parseInt($status.dataset.status) + 1
                            $status.dataset.status = parseInt($status.dataset.status) + 1 >= 7 ? 6 :  parseInt($status.dataset.status) + 1
                        },100)
                    }
                
                }else if(ev.deltaY < 0 && cmd.status > 0){
                    if($status.dataset.status == 1 && second.scrollTop == 0 || $status.dataset.status == 2 && terceiro.scrollTop == 0){
                        cmd.new = parseInt($status.dataset.status) - 1;
                        cmd.prev();
                        if((parseInt($status.dataset.status) - 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
                    
                        setTimeout(()=>{
                            cmd.status = parseInt($status.dataset.status) - 1 < 0 ? 0 :  parseInt($status.dataset.status) - 1
                            $status.dataset.status = parseInt($status.dataset.status) - 1 < 0 ? 0 :  parseInt($status.dataset.status) - 1
                        },100)
                    }else if($status.dataset.status == 2 && terceiro.scrollTop <= 0){
                        cmd.new = parseInt($status.dataset.status) - 1;
                        cmd.prev();
                        if((parseInt($status.dataset.status) - 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
                    
                        setTimeout(()=>{
                            cmd.status = parseInt($status.dataset.status) - 1 < 0 ? 0 :  parseInt($status.dataset.status) - 1
                            $status.dataset.status = parseInt($status.dataset.status) - 1 < 0 ? 0 :  parseInt($status.dataset.status) - 1
                        },100)
                    }else if($status.dataset.status != 1  && $status.dataset.status != 2){
                        cmd.new = parseInt($status.dataset.status) - 1;
                        cmd.prev();
                        if((parseInt($status.dataset.status) - 1) > 0 ){
                            document.querySelector(".mouse_scroll").style = "opacity: 0;"
                        }else{
                            document.querySelector(".mouse_scroll").style = "opacity: 1;"
                        }
                    
                        setTimeout(()=>{
                            cmd.status = parseInt($status.dataset.status) - 1 < 0 ? 0 :  parseInt($status.dataset.status) - 1
                            $status.dataset.status = parseInt($status.dataset.status) - 1 < 0 ? 0 :  parseInt($status.dataset.status) - 1
                        },100)
                    }
                
                }
                    order = false;
            }, 500)
        }
    }
    
});