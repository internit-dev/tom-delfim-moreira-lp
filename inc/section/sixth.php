<section id="contato" class="js-section-effect">
    <div class="container-fluid tom ">
        <div class="row tom_content">
            <div class="col-12 collum-mob">
                <div class="tom_development-internit">
                    <div class="tom_development-internit-logo">
                        <a href="https://internit.com.br" target="_blank">
                            <img src="./assets/src/img/logo/internit.png" alt="Logotipo da Internit">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum px-0">
                            <div class="tom_development-carousel">
                                <div class="swiper-container swiper-background-carousel">
                                    <!-- Wrapper adicional necessário -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                            <div class="swiper-img">
                                                <a>
                                                    <img src="./assets/src/img/background-section.png" alt="Fotografia">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="tom_development-box">
                                <div class="tom_development-box-logo">
                                    <div class="tom_development-box-logo-img">
                                        <img src="./assets/src/img/logo/gafisa.png" alt="Logotipo da Gafisa">
                                    </div>
                                </div>
                                <div class="tom_development-box-logo">
                                    <div class="tom_development-box-logo-img">
                                    <img src="./assets/src/img/logo/viverbem.png" alt="Logotipo da Gafisa">
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="contact" class="col-12 col-md-6 tom_content-collum  px-0 px-3">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 col-md-10 tom_development-collum  px-0 px-3">
                           <div class="tom_development-info">
                                <div class="tom_development-title">
                                    <h2>Entre em contato e saiba mais.</h2>
                                </div>
                                <div class="tom_development-text">
                                    <p>Contact us to learn more</p>
                                </div>
                           </div>
                            <div class="tom_development-form">
                                <?php ############################################################################################################# ?>
                                <?php $formName = 'principal'; ?>
                                <form action="" name="<?= $formName ?>" id="form-<?= $formName ?>">
                                    
                                     <!-- input nome -->
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group">
                                                <input name="Nome" type="text" class="" placeholder="Nome*" required>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <!-- input email -->
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group">
                                                <input name="Email" type="email" class="" placeholder="E-mail*" required>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <!-- input telefone -->
                                    <div class="form-row">
                                        <div class="col">
                                            <div class="form-group">
                                                <input name="Telefone" type="tel" class="js-input-celular"  required>
                                            </div>
                                        </div>
                                    </div>
                                    

                                    <!-- input Mensagem -->
                                    <div class="form-group">
                                        <select class="custom-select" name="Mensagem" required>
                                            <option value="">Como prefere ser contactado?</option>
                                            <option value="telefone">Telefone</option>
                                            <option value="email">E-mail</option>
                                            <option value="whatsapp">WhatsApp</option>
                                        </select>
                                        <div class="invalid-feedback">Escolha inválida, selecione uma das opções válidas</div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col form-terms">
                                            <div class="form-group d-flex">
                                                <input name="Termos" type="radio"  value="aceito" required> <label>Li e aceito os termos de uso dos dados conforme indicado na <a data-toggle="modal" data-target="#ModalLong"><strong>Política de Privacidade</strong></a>.</label>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- botão enviar -->
                                    <div class="button">
                                        <button id="form-botao-<?= $formName ?>" type="submit" class="">Enviar</button>
                                    </div>

                                </form>
                                <?php unset($formName); ?>
                                <?php ############################################################################################################# ?>
                            </div>
                            <div class="tom_development-internit">
                                <div class="tom_development-internit-logo">
                                    <a href="https://internit.com.br" target="_blank">
                                        <img src="./assets/src/img/logo/internit.png" alt="Logotipo da Internit">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>