<main id="primary" class="site-main js-section-effect">
    <div class="container-fluid tom ">
        <div class="row tom_content">
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum px-0">
                            <div class="tom_development-carousel">
                                <div class="swiper-container swiper-main-carousel">
                                    <!-- Wrapper adicional necessário -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                            <div class="swiper-img">
                                                <a>
                                                    <img src="./assets/src/img/background-main.png" class="d-md-none" alt="Paisagem">
                                                    <iframe width="560" height="315" class="d-none d-md-block" src="https://www.youtube-nocookie.com/embed/45hTGKNFFbM?autoplay=1&amp;loop=1&amp;mute=1&amp;playlist=45hTGKNFFbM&amp;controls=0" title="YouTube video player" frameborder="0" allow="ccelerometer; autoplay; calipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                </a>
                                                <div class="filter"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tom_development-video">
                                        <a data-fslightbox="html5-youtube-videos" href="https://www.youtube.com/embed/RwPvwlIlSSo">
                                            <img src="./assets/src/img/logo/logo-lebron.png" alt="Logo lebron">
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="container">
                    <div class="row tom_development">
                        <div class="col-12 col-md-10 tom_development-collum">
                            <div class="tom_development-info">
                                <div class="tom_development-title">
                                    <h2>No último terreno da<br> Delfim Moreira, uma obra
                                        <br>de arte Gafisa.
                                    </h2>
                                </div>
                                <div class="tom_development-text">
                                    <p>In the last plot of land in Delfim Moreira,<br> a work of art by Gafisa</p>
                                </div>
                            </div>
                            <div class="tom_development-imgs">
                                <div class="tom_development-bloc">
                                    <div class="tom_development-bloc-one">
                                        <a>
                                            <img src="./assets/src/img/fotografia-lazer.png" alt="Fotografia">
                                        </a>
                                    </div>
                                </div>
                                <div class="tom_development-box">
                                    <div class="tom_development-box-two">
                                        <a>
                                            <img src="./assets/src/img/fotografia-calcadao.png" alt="Fotografia">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-two">
                                        <a>
                                            <img src="./assets/src/img/fotografia-praia.png" alt="Fotografia">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>