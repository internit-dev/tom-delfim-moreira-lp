<section id="arte" class="js-section-effect">
    <div class="container-fluid tom ">
        <div class="row tom_content">
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum js-box-scroll-2">
                            <div class="tom_development-box reverse">
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>IOLE DE FREITAS</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>As esculturas da artista plástica Iole de Freitas ocuparão o canteiro em frente ao edifício, refletindo, em seu movimento pelo espaço, as linhas da fachada e tornando-se um presente à cidade do Rio de Janeiro devido à sua dimensão pública</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/iole-de-freitas.png" alt="Iole de Freitas">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-illustration-name">
                                        <p>"As 3 Graças" da artista Iole de Freitas</p>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box reverse">
                                <div class="tom_development-btox-illusration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/ernesto-neto.png" alt="ERNESTO NETO">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-illustration-name">
                                        <p>Foto: Pepe Schelttino | Contesia<br> Galeria fontes D'Aloia & Gabriel</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>ERNESTO NETO</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Já com o artista carioca Ernesto Neto, a ideia foi de trazer uma obra que alterasse o espaço. Os trabalhos dele trazem uma energia muito positiva e provocam as pessoas a se relacionarem com elas.</p>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box">
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>VIK MUNIZ</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Formadas por fotos em p&b e sépia retiradas de recordações de famílias, as imagens da série album são elas mesmas enormes reproduções de cenas pessoais imortalizadas pela câmera.</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/vik-muniz.png" alt="VIK MUNIZ">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-illustration-name">
                                        <p>A tela "Cable Car", de Vik Muniz</p>
                                    </div>
                                </div>
                            </div> 
                            <div class="tom_development-box">
                                <div class="tom_development-box-illustration">
                                    <div class="tom_development-box-illustration-img">
                                        <a>
                                            <img src="./assets/src/img/artista/irmaos-campana.png" alt="IRMÃOS CAMPANA">
                                        </a>
                                    </div>
                                    <div class="tom_development-box-illustration-name">
                                        <p>Retrato: Bob Wolfenson</p>
                                    </div>
                                </div>
                                <div class="tom_development-box-describe">
                                    <div class="tom_development-box-describe-title">
                                        <h3>IRMÃOS CAMPANA</h3>
                                    </div>
                                    <div class="tom_development-box-describe-text">
                                        <p>Criados em 2017, o cobogó "Mão" foi desenhado pelo instituto Campana em parceria com a Divina Terra Revestimentos. Em 2019, fez parte da exposição "Irmão Campana: 35 Revoluções" no MAM do Rio.</p>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum">
                            <div class="tom_development-title">
                                <h1>A arte dá o Tom.</h1>
                            </div>
                            <div class="tom_development-text">
                                <p>Art sets the tone</p>
                            </div>
                            <div class="tom_development-info">
                                <p>O primeiro empreendimento que também é uma galeria de 
                                arte do Rio com obras de artistas contemporâneos como Iole
                                de Freitas, Ernesto Neto, Vik Muniz e os irmãos Compana.</p>
                            </div>
                            <div class="tom_development-img">
                                <a data-fslightbox="fachada" href="./assets/src/img/sala-fotografia.png">
                                    <img src="./assets/src/img/sala-fotografia.png" alt="Fotografia">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>               
</section>