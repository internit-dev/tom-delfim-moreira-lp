<!DOCTYPE html>
<html lang="pt_BR">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tom Delfim Moreira | Uma obra de arte da Gafisa</title>

  <link rel="shortcut icon" href="./assets/src/img/favicon.svg">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.0/css/all.min.css" integrity="sha512-3PN6gfRNZEX4YFyz+sIyTF6pGlQiryJu9NlGhu9LrLMQ7eDjNgudQoFDK3WSNAayeIKc6B8WXXpo4a7HqxjKwg==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <meta name="description" content="No último terreno da Delfim Moreira, uma obra de arte da Gafisa.">
  <meta name="robots" content="all">
  <meta name="author" content="Internit">
  <meta name="keywords" content="Apartamento leblon, apartamento delfim moreira, delfim moreira, leblon, tom, gafisa">

  <meta property="og:type" content="website">
  <meta property="og:url" content="https://tomdelfimmoreira.com.br/">
  <meta property="og:title" content="Tom Delfim Moreira | Uma obra de arte da Gafisa">
  <meta property="og:image" content="https://tomdelfimmoreira.com.br/assets/src/img/favicon.svg">
  <meta property="og:description" content="No último terreno da Delfim Moreira, uma obra de arte da Gafisa.">
  <script>
    function goToForm() {
     let link = document.createElement('a');
     link.setAttribute('href', './#contato');
     link.click();
    }
  </script>

<!-- Hotjar Tracking Code for https://tomdelfimmoreira.com.br/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:2827562,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

  <!-- Style -->
  <link rel="stylesheet" href="./assets/dist/css/template-globals.css">

  <!-- Jquery -->
  <script src="./assets/dist/js/libary/jquery/jquery-3.6.0.min.js"></script>

  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-WGX54LM');</script>
  <!-- End Google Tag Manager -->


   <!-- INTEGRAÇÃO HEADER -->
  <?php require_once 'integracao/integracaoHead.php'; ?>

</head>
<body>
<div class="box-comercial row">
            <div class="col-6" onclick='goToForm()'> 
                    <i class='far fa-envelope' style='font-size:24px;font-display:swap'></i>
                    <span>Fale Conosco</span>
            </div>
            <div class="col-6" onclick="javascript:window.open('https:\/\/api.whatsapp.com/send?phone=5521965470005&text=Ol%C3%A1%2C%20gostaria%20de%20saber%20mais%20informa%C3%A7%C3%B5es%20sobre%20o%20empreendimento%20Tom%20Delfim%20Moreira', '_blank')">
                    <svg width="26" height="26" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M30.1031 5.31481C26.8536 2.03715 22.526 0.234436 17.927 0.234436C8.43431 0.234436 0.709854 8.00717 0.709854 17.5592C0.709854 20.6105 1.50091 23.5916 3.00547 26.2216L0.5625 35.1961L9.69069 32.7847C12.2035 34.166 15.0342 34.8918 17.9193 34.8918H17.927C27.412 34.8918 35.307 27.119 35.307 17.567C35.307 12.9393 33.3526 8.59246 30.1031 5.31481ZM17.927 31.9731C15.3522 31.9731 12.8317 31.2785 10.6369 29.9675L10.1172 29.6553L4.70392 31.0834L6.14644 25.769L5.8052 25.2227C4.37044 22.9283 3.61816 20.2828 3.61816 17.5592C3.61816 9.62258 10.0397 3.16092 17.9348 3.16092C21.7582 3.16092 25.349 4.65927 28.0479 7.38285C30.7468 10.1064 32.4065 13.7197 32.3987 17.567C32.3987 25.5114 25.8143 31.9731 17.927 31.9731ZM25.7755 21.188C25.349 20.9695 23.2318 19.9238 22.8362 19.7833C22.4407 19.635 22.1537 19.5648 21.8668 20.0018C21.5798 20.4389 20.7578 21.4065 20.5018 21.7031C20.2537 21.9918 19.9977 22.0309 19.5712 21.8123C17.0429 20.5403 15.3832 19.5414 13.7158 16.6617C13.2737 15.897 14.1578 15.9516 14.9799 14.2971C15.1195 14.0084 15.0497 13.7587 14.9411 13.5402C14.8326 13.3217 13.9717 11.1912 13.615 10.3249C13.266 9.48211 12.9092 9.59917 12.6455 9.58356C12.3974 9.56796 12.1104 9.56796 11.8234 9.56796C11.5365 9.56796 11.0712 9.67721 10.6756 10.1064C10.2801 10.5434 9.17108 11.5892 9.17108 13.7197C9.17108 15.8501 10.7144 17.9104 10.9238 18.1991C11.141 18.4879 13.9562 22.8581 18.276 24.7388C21.0059 25.925 22.0762 26.0265 23.4412 25.8236C24.271 25.6987 25.9849 24.7778 26.3417 23.7633C26.6985 22.7488 26.6985 21.8826 26.5899 21.7031C26.4891 21.508 26.2021 21.3987 25.7755 21.188Z" fill="white"></path>
                    </svg>
                    <span>Whatsapp</span>
            </div>
        </div>
