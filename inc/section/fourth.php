<section id="plantas" class="js-section-effect">
    <div class="container-fluid tom">
        <div class="row tom_content">
        <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development justify-content-center justify-content-xl-end">
                        <div class="col-11 col-md-10 tom_development-collum">
                            <div class="filter-mb d-md-none"></div>
                           <div class="tom_development-carousel">
                                <div class="swiper-container swiper-planta-carousel">
                                    <!-- Wrapper adicional necessário -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>APARTAMENTO 101 (ÁREA PRIVATIVA TOTAL - 315,60m²)</span>
                                            </div>
                                            <a data-fslightbox="gallery-plants" href="./assets/src/img/plantas/APARTAMENTO-101.jpg">
                                                <img src="./assets/src/img/plantas/planta-1.png" alt="Planta">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>APTOS 201 A 501 - (ÁREA PRIVATIVA TOTAL - 283,96m²)</span>
                                            </div>
                                            <a data-fslightbox="gallery-plants" href="./assets/src/img/plantas/APTOS-201-A-501.jpg">
                                                <img src="./assets/src/img/plantas/planta-2.png" alt="Planta">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>COBERTURA 601 - (ÁREA PRIVATIVA TOTAL - 501,56m²)</span>
                                            </div>
                                            <a data-fslightbox="gallery-plants" href="./assets/src/img/plantas/COBERTURA-601.jpg">
                                                <img src="./assets/src/img/plantas/planta-3.png" alt="Planta">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>DEPENDÊNCIA 601 - (ÁREA PRIVATIVA TOTAL - 501,56m²)</span>
                                            </div>
                                            <a data-fslightbox="gallery-plants" href="./assets/src/img/plantas/DEPENDENCIA-601.jpg">
                                                <img src="./assets/src/img/plantas/planta-4.png" alt="Planta">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                           </div>
                           <div class="tom_development-thumbnail">
                                <div class="swiper-container swiper-thumbnail-planta-carousel">
                                    <!-- Wrapper adicional necessário -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                      <!-- Slides -->
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/plantas/planta-102.png" alt="Planta">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/plantas/planta-101.png" alt="Planta">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/plantas/planta-103.png" alt="Planta">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/plantas/planta-104.png" alt="Planta">
                                        </div>
                                    </div>
                                </div>
                                 <!-- Caso precise de navegação por botões -->
                                 <div class="swiper-thumbnail-planta-button-prev">
                                    <img src="./assets/src/img/arrow-right.png" alt="Arrow Right">
                                </div>
                                <div class="swiper-thumbnail-planta-button-next">
                                    <img src="./assets/src/img/arrow-left.png" alt="Arrow Left">
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development ">
                        <div class="col-12 col-md-10 tom_development-collum">
                           <div class="tom_development-info">
                                <div class="tom_development-title">
                                    <h1>
                                       A arte de viver é uma<br> planta em branco onde<br> você dá o Tom.</h1>
                                </div>
                                <div class="tom_development-text">
                                    <p>Art of living is a blank flo<span>.</span>or plan where<br> you set the tone</p>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>               
</section>