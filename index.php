<?php

/**
 * The main template file
 *
 * Este é o modelo de landing page base da internit, desenvolvedora web. 
 * Nele você encontrará recursos para construir totalmente uma landing page 
 * sem precisar baixar nada.
 *
 * @package internit
 */

require './header.php';

?>
<style>
    .lupa svg {
        height: 30px;
        margin-top: 5px;
    }

    .menu_content-contact {
        margin: 0 20px;
    }

    a,
    a:link,
    a:visited,
    a:hover,
    a:active {
        color: white;
        text-decoration: none;
    }

    /*@media (pointer:coarse) {
        #navbar_contact_button {
            display:none;
        }
    }*/

    /*  @media screen and (max-width: 550px) {
        .menu_content-contact {
            margin: 5px;
        }
    }
    @media screen and (max-width: 340px) {
        #navbar_list {
            flex-direction: row;
            top: 100px;
            right:auto;
        }
    }
    @media screen and (min-width: 341px) and (max-width:550px) {
        #navbar_list {
            flex-direction: column;
           right:8%;
            top: 16%;
        }
    }
    @media screen and (min-width: 551px) {
        #navbar_list {
            flex-direction: row;
            right:8%;
            top: 35px;
        }
    } */

    @media screen and (min-width: 1550px) {
        .menu_content-contact-text span {
            font-size: 0.75rem !important;
        }
    }
</style>
<div class="water_mark">
    <div class="water_mark-img">
        <img src="./assets/src/img/logo/Logotipo_branco.svg" alt="Logotipo">
    </div>
</div>
<!-- <div style="position:absolute;z-index:999;right:10px;bottom:30px">
    <img src="./assets/src/img/wpp_logo.png" style="width:48px;height:48px;position:sticky;bottom:30px;right:10px" alt="">
</div> -->

<!-- Seção Principal -->
<?php require './inc/main/index.php' ?>

<!-- Primeira Seção -->
<?php require './inc/section/first.php' ?>

<!-- Segunda Seção -->
<?php require './inc/section/second.php' ?>

<!-- Terceira Seção -->
<?php require './inc/section/third.php' ?>

<!-- Quarta Seção -->
<?php require './inc/section/fourth.php' ?>

<!-- Quinta Seção -->
<?php require './inc/section/fifth.php' ?>

<!-- Quinta Seção -->
<?php require './inc/section/sixth.php' ?>

<!--<div id="navbar_list" style="position:absolute;z-index:999"> -->

<!-- <div class="menu_content-contact">
                    <a href="#contato">
                        <div class="menu_content-contact-text" style="background-color:#969696">
                            <span id="navbar_contact_button" style='font-size:0.5rem'>Contato</span>
                        </div>
                    </a>
                </div> -->
<!--</div> -->
<menu>
    <div class="menu_content">

        <div id="navbar_list" class="d-flex justify-content-around" style="position:fixed;z-index:999;right:8%; top:35px">
            <div class="menu_content-contact">
                <a class="d-block" href="https://api.whatsapp.com/send?phone=5521996224850&text=Ol%C3%A1%2C%20gostaria%20de%20saber%20mais%20informa%C3%A7%C3%B5es%20sobre%20o%20Tom%20Delfim%20Moreira1" target="_blank">
                    <div id="wpp-botao" class="menu_content-contact-text" style="color:white;background-color:#4ca932;width:127px;border-radius: 1rem 0rem 1rem 0;text-align:center">
                        <img src="./assets/src/img/wpp_logo.png" style="width:16px;height:16px" alt="wpp_logo">
                        <p style="font-size:0.5rem;display:inline">WhatsApp</p>
                    </div>
                </a>
            </div>
        </div>
        <div class="menu_content-close" style="position:relative;top:-10px">
            <div class="menu_content-close-itens">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>

        <div class="menu_content-navbar">
            <nav data-status="0">
                <ul>
                    <li>
                        <a href="#primary" data-section="0">1<p>.</p>
                            <span style="opacity: 1;transform: translateY(-16px);">Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="#arquitetura" data-section="1">2<p>.</p>
                            <span>Arquitetura</span>
                        </a>
                    </li>
                    <li>
                        <a href="#arte" data-section="2">3<p>.</p>
                            <span>Arte</span>
                        </a>
                    </li>
                    <li>
                        <a href="#galeria" data-section="3">4<p>.</p>
                            <span>Galeria</span>
                        </a>
                    </li>
                    <li>
                        <a href="#plantas" data-section="4">5<p>.</p>
                            <span>Plantas</span>
                        </a>
                    </li>
                    <li>
                        <a href="#realizacao" data-section="5">6<p>.</p>
                            <span>Realização</span>
                        </a>
                    </li>
                    <li>
                        <a href="#contato" data-section="6">7<p>.</p>
                            <span>Contato</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="mouse_scroll">
            <div class="mouse">
                <div class="wheel"></div>
            </div>
            <div class="itens_mouse">
                <span class="m_scroll_arrows unu"></span>
                <span class="m_scroll_arrows doi"></span>
                <span class="m_scroll_arrows trei"></span>
            </div>
        </div>
        <div class="menu_content-download">
            <div class="menu_content-download-text">
                <span>Donwload do book:</span>
            </div>
            <div class="menu_content-download-books">
                <a href="./assets/src/img/Book Digital - Tom Delfim Moreira.pdf" download>
                    <img src="./assets/src/img/baixar.png" alt="Icon de donwload">
                    <span>PT-br</span>
                </a>
                <a href="" download style="display: none">
                    <img src="./assets/src/img/baixar.png" alt="Icon de donwload">
                    <span>EN-us</span>
                </a>
            </div>
        </div>
    </div>
</menu>

<?php

require './footer.php';
