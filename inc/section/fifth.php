<section id="realizacao" class="js-section-effect">
    <div class="container-fluid tom ">
        <div class="row tom_content">
            <div class="col-12 col-md-6 tom_content-collum ">
                <div class="content">
                    <div class="row tom_development justify-content-md-end">
                        <div class="col-12 col-md-10 tom_development-collum ">
                           <div class="tom_development-info">
                                <div class="tom_development-info-box-logo">
                                    <div class="tom_development-info-box-logo-img">
                                        <img src="./assets/src/img/logo/gafisa-white.png" alt="Logotipo da Gafisa">
                                    </div>
                                </div>
                                <div class="tom_development-info-title">
                                    <h2>Mais que empreendimentos,<br> construímos uma cidade</h2>
                                </div>
                                <div class="tom_development-info-list">
                                    <div class="tom_development-info-list-text">
                                        <span>MAIS DE</span>
                                        <h3>1,5 MILHÃO</h3>
                                        <span>DE CLIENTES VIVENDO EM UM GAFISA</span>
                                    </div>
                                    <div class="tom_development-info-list-icon">
                                        <img src="./assets/src/img/icon-familias.png" alt="Icon Familias">
                                    </div>
                                </div>
                                <div class="tom_development-info-list">
                                    <div class="tom_development-info-list-text">
                                        <span>MAIS DE</span>
                                        <h3>1,6 MILHÃO DE M²</h3>
                                        <span>CONSTRUÍDOS</span>
                                    </div>
                                    <div class="tom_development-info-list-icon">
                                        <img src="./assets/src/img/icon-construcoes.png" alt="Icon Construcoes">
                                    </div>
                                </div>
                                <div class="tom_development-info-list">
                                    <div class="tom_development-info-list-text pt-3">
                                        <h3>18 VEZES</h3>
                                        <span>PREMIADA</span>
                                    </div>
                                    <div class="tom_development-info-list-icon">
                                        <img src="./assets/src/img/icon-premeada.png" alt="Icon Premeada">
                                    </div>
                                </div>
                                <div class="tom_development-info-list">
                                    <div class="tom_development-info-list-text">
                                        <span>MAIS DE</span>
                                        <h3>1.200</h3>
                                        <span>EMPREENDIMENTOS ENTREGUES</span>
                                    </div>
                                    <div class="tom_development-info-list-icon">
                                        <img src="./assets/src/img/icon-predios.png" alt="Icon Prédios">
                                    </div>
                                </div>
                                <div class="tom_development-info-list">
                                    <div class="tom_development-info-list-text">
                                        <span>PRESENÇA EM</span>
                                        <h3>40 CIDADES DE 19 ESTADOS</h3>
                                        <span>EM TODO O BRASIL</span>
                                    </div>
                                    <div class="tom_development-info-list-icon">
                                        <img src="./assets/src/img/icon-brasil.png" alt="Icon Brasil"> 
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 tom_content-collum px-0">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum px-0">
                            <div class="tom_development-carousel">
                                <div class="swiper-container swiper-background-carousel">
                                    <!-- Wrapper adicional necessário -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                            <div class="swiper-img">
                                                <a>
                                                    <img src="./assets/src/img/rio-de-janeiro.png" alt="Fotografia">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>