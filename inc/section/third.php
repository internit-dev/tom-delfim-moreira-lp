<section id="galeria" class="js-section-effect">
    <div class="container-fluid tom">
        <div class="row tom_content">
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development justify-content-end">
                        <div class="col-12 col-md-10 tom_development-collum">
                            <div class="tom_development-info">
                                <div class="tom_development-title">
                                    <h2>Apenas 6 unidades.<br>
                                        Um por andar,<br> todas com 4 suítes.</h2>
                                </div>
                                <div class="tom_development-text">
                                    <p>Only 6 units. One per flo<span>.</span>or, all units<br>win four en suite bathro<span>.</span>oms</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 tom_content-collum">
                <div class="content">
                    <div class="row tom_development">
                        <div class="col-12 tom_development-collum">
                           <div class="tom_development-carousel">
                                <div class="swiper-container swiper-galeria-imovel">
                                    <!-- Wrapper adicional necessário -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Sala Panoramica</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/1-Sala-Panoramica.jpg">
                                                <img src="./assets/src/img/perspectivas/1-Sala-Panoramica.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Sala Cobertura</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/2-Sala-Cobertura.jpg">
                                                <img src="./assets/src/img/perspectivas/2-Sala-Cobertura.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Sala de Jantar</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/3-Sala-de-Jantar.jpg">
                                                <img src="./assets/src/img/perspectivas/3-Sala-de-Jantar.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Piscina Cobertura</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/4-Piscina-Cobertura.jpg">
                                                <img src="./assets/src/img/perspectivas/4-Piscina-Cobertura.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Sala Decorado</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/5-Sala-Decorada.jpg">
                                                <img src="./assets/src/img/perspectivas/5-Sala-Decorada.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Suíte Decorado</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/6-Suite-Decorada.jpg">
                                                <img src="./assets/src/img/perspectivas/6-Suite-Decorada.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Fachada Detalhe</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/7-Fachada.jpg">
                                                <img src="./assets/src/img/perspectivas/7-Fachada.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Acesso</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/8-Acesso.jpg">
                                                <img src="./assets/src/img/perspectivas/8-Acesso.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Lavabo Térreo</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/11-Lavabo-Terreo.jpg">
                                                <img src="./assets/src/img/perspectivas/11-Lavabo-Terreo.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Terraço Cobertura</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/12-Cobertura.jpg">
                                                <img src="./assets/src/img/perspectivas/12-Cobertura.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Vista Mar</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/13-Vista-Mar.jpg">
                                                <img src="./assets/src/img/perspectivas/13-Vista-Mar.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Varanda Decorado</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/14-Varanda.jpg">
                                                <img src="./assets/src/img/perspectivas/14-Varanda.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Academia</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/15-Academia.jpg">
                                                <img src="./assets/src/img/perspectivas/15-Academia.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Pranchário</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/16-Pranchario.jpg">
                                                <img src="./assets/src/img/perspectivas/16-Pranchario.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Sala Ampliada</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/17-Sala-Ampliada.jpg">
                                                <img src="./assets/src/img/perspectivas/17-Sala-Ampliada.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Sala de Motorista</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/18-Sala-de-Motorista.jpg">
                                                <img src="./assets/src/img/perspectivas/18-Sala-de-Motorista.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Sala de Reunião</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/19-Sala-de-Reuniao.jpg">
                                                <img src="./assets/src/img/perspectivas/19-Sala-de-Reuniao.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Suíte Reflexo</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/20-Suite.jpg">
                                                <img src="./assets/src/img/perspectivas/20-Suite.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="swiper-slide-name">
                                                <span>Terraço Garden</span>
                                            </div>
                                            <a data-fslightbox="gallery-imovel" data-type="image" href="./assets/src/img/perspectivas/21-Terraco-Garden.jpg">
                                                <img src="./assets/src/img/perspectivas/21-Terraco-Garden.jpg" alt="Fotografia">
                                            </a>
                                        </div>
                                    </div>
                                    
                                </div>
                                  <!-- Caso precise de navegação por botões -->
                                  <div class="swiper-imovel-button-prev d-md-none">
                                        <img src="./assets/src/img/arrow-right.png" alt="Arrow Right">
                                    </div>
                                    <div class="swiper-imovel-button-next d-md-none">
                                        <img src="./assets/src/img/arrow-left.png" alt="Arrow Left">
                                    </div>
                           </div>
                           <div class="tom_development-thumbnail">
                                <div class="swiper-container swiper-thumbnail-galeria-imovel">
                                    <!-- Wrapper adicional necessário -->
                                    <div class="swiper-wrapper">
                                        <!-- Slides -->
                                        <div class="swiper-slide">
                                                <img src="./assets/src/img/perspectivas/1-Sala-Panoramica.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                                <img src="./assets/src/img/perspectivas/2-Sala-Cobertura.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/3-Sala-de-Jantar.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/4-Piscina-Cobertura.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/5-Sala-Decorada.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/6-Suite-Decorada.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/7-Fachada.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/8-Acesso.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/11-Lavabo-Terreo.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/12-Cobertura.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/13-Vista-Mar.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/14-Varanda.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/15-Academia.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/16-Pranchario.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/17-Sala-Ampliada.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/18-Sala-de-Motorista.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/19-Sala-de-Reuniao.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/20-Suite.jpg" alt="Fotografia">
                                        </div>
                                        <div class="swiper-slide">
                                            <img src="./assets/src/img/perspectivas/21-Terraco-Garden.jpg" alt="Fotografia">
                                        </div>
                                    </div>
                                </div>
                                <!-- Caso precise de navegação por botões -->
                                <div class="swiper-thumbnail-imovel-button-prev">
                                    <img src="./assets/src/img/arrow-right.png" alt="Arrow Right">
                                </div>
                                <div class="swiper-thumbnail-imovel-button-next">
                                    <img src="./assets/src/img/arrow-left.png" alt="Arrow Left">
                                </div>
                           </div>
                           <div class="tom_development-text">
                               <p>Todas as imagens e plantas são representações artísticas, meramente ilustrativas, podendo sofrer alteração de cor, formato, textura, posicionamento, metragem e acabamento, conforme o projeto. Projeto Legal aprovado de acordo com o processo 02/11/000.407/2020, com Memorial de Incorporação já registrado junto ao 2º RGI da Comarca da Capital do Estado do Rio de Janeiro. PREO: Henrique Serrano da Costa Moreira CREA – 201417727-9 . PRPA: Sergio Gattás CAU A1363-3, Elizabeth Vasquez Guedes CAU A7247-8. Processo de adoção e legalização do canteiro onde a obra de arte será alocada se encontra em trâmite de aprovação junto aos órgãos competentes. *A denominação "Tom - Delfim Moreira" é exclusivamente para fins comerciais. Quando concluído, o empreendimento se intitulará "Condomínio do Edifício J. A. Villella Pedras".
                                </p>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>               
</section>